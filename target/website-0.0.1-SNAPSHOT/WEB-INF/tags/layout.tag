<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Feisty Felines Breedables - ${title}
    </title>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/main.css'/>"/>
	
	<script src="<c:url value='/js/jquery-1.12.0.min.js'/>"></script>
	<script src="<c:url value='/js/bootstrap.js'/>"></script>
	<script src="<c:url value='/js/main.js'/>"></script>
	
</head>
<body>
		<div class="container">
			<div class="row">
				<div class="col-md-5" id="slideshow">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img src="<c:url value='/images/carousel.png'/>" style="height:150px;">
							</div>
							<div class="item">
								<img src="<c:url value='/images/Abyssinian.png'/>" style="height:150px;"/>
                             </div>
                             <div class="item">
                             	<img src="<c:url value='/images/Snapshot 2, Abyssinian Sparkle Sapphire_002.png'/>" style="height:150px;">
                             </div>
                             <div class="item">
                             	<img src="<c:url value='/images/Garden Statue_003.png'/>" style="height:150px;">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-7" id="header">
					<img src="<c:url value='/images/header.png'/>" style="height:150px;"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="row">
						<div class="col-md-12 layout-item">
							<ul class="nav nav-pills nav-stacked" id="pills-custom">
								<c:choose>
									<c:when test="${active == 'home' }">
										<li role="presentation" class="active"><a href="<c:url value='/'/>">Home</a></li>
									</c:when>
									<c:otherwise>
										<li role="presentation" class=""><a href="<c:url value='/'/>">Home</a></li>
									</c:otherwise>
								</c:choose>
								
								<c:choose>
									<c:when test="${active == 'lore' }">
										<li role="presentation" class="active"><a href="<c:url value='/Pages/Lore'/>">Lore</a></li>
									</c:when>
									<c:otherwise>
										<li role="presentation" class=""><a href="<c:url value='/Pages/Lore'/>">Lore</a></li>
									</c:otherwise>
								</c:choose>
								
								<c:choose>
									<c:when test="${active == 'support' || active == 'manual' }">
										<li role="presentation" class="dropdown active">
									</c:when>
									<c:otherwise>
										<li role="presentation" class="dropdown">
									</c:otherwise>
								</c:choose>
								<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
										Support <span class="caret"></span>
									</a>
									<ul class="dropdown-menu my-dropdown-right">
											<li><a href="<c:url value='/Pages/Manual'/>">Manual</a></li>										
											<li><a href="<c:url value='/Account/Tickets'/>">Tickets</a></li>
										
									</ul>
								</li>
								
								<c:choose>
									<c:when test="${active == 'pelts' }">
										<li role="presentation" class="dropdown active">
									</c:when>
									<c:otherwise>
										<li role="presentation" class="dropdown">
									</c:otherwise>
								</c:choose>
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
										Pelts <span class="caret"></span>
									</a>
									<ul class="dropdown-menu my-dropdown-right">
										<c:set var="path" value ="/Traits/Pelt Color/"/>
										<c:forEach var="peltname" items="${peltNames}" varStatus="counter">
											<li><a href="<c:url value='${path}${peltname}'/>">${peltname}</a></li>
										</c:forEach>
									</ul>
								</li>
								<c:choose>
									<c:when test="${active == 'traits' }">
										<li role="presentation" class="dropdown active">
									</c:when>
									<c:otherwise>
										<li role="presentation" class="dropdown">
									</c:otherwise>
								</c:choose>

									<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
										Traits <span class="caret"></span>
									</a>
									<ul class="dropdown-menu my-dropdown-right">
										<li><a href="<c:url value='/Traits/Body/'/>">Body</a></li>
										<li><a href="<c:url value='/Traits/Ears/'/>">Ears</a></li>
										<li><a href="<c:url value='/Traits/Eye Color/'/>">Eye Color</a></li>
										<li><a href="<c:url value='/Traits/Eye Shape/'/>">Eye Shape</a></li>
										<li><a href="<c:url value='/Traits/Pupil Shape/'/>">Pupil Shape</a></li>
										<li><a href="<c:url value='/Traits/Tail/'/>">Tail</a></li>
										<li><a href="<c:url value='/Traits/Whiskers/'/>">Whiskers</a></li>
									</ul>
								<c:choose>
									<c:when test="${active == 'tips' }">
										<li role="presentation" class="active"><a href="/Pages/Tips">Tips & Info</a></li>
									</c:when>
									<c:otherwise>
										<li role="presentation" class=""><a href="#">Tips & Info</a></li>
									</c:otherwise>
								</c:choose>
								<c:if test="${pageContext.request.userPrincipal.name == null}">
								<c:choose>
									<c:when test="${active == 'login' }">
										<li role="presentation" class="active"><a href="<c:url value='/login'/>">Login</a></li>
									</c:when>
									<c:otherwise>
										<li role="presentation" class=""><a href="<c:url value='/login'/>">Login</a></li>
									</c:otherwise>
								</c:choose>
								</c:if>
								<c:if test="${pageContext.request.userPrincipal.name != null}">
								<form action="<c:url value='/logout' />" method="post" id="logoutForm">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<script>
									function formSubmit() {
										document.getElementById("logoutForm").submit();
									}
								</script>
								<c:choose>
									<c:when test="${active == 'account' }">
										<li role="presentation" class="dropdown active">
									</c:when>
									<c:otherwise>
										<li role="presentation" class="dropdown">
									</c:otherwise>
								</c:choose>

									<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
										Account <span class="caret"></span>
									</a>
									<ul class="dropdown-menu my-dropdown-right">
										<li><a href="<c:url value='/Account/Profile'/>">Profile</a>
										<li><a href="<c:url value='/Account/Cats'/>">Cats</a>
										<li><a href="javascript:formSubmit()">Logout</a></li>
										<sec:authorize access="hasAuthority('admin')">
											<li><a href="<c:url value='/Admin/Images'/>">Images</a></li>
										</sec:authorize>
									</ul>
								
								</c:if>

								
							</ul>
						</div>
						<div class="col-md-12 layout-item">
							<a href="<c:url value='/Account/Tickets/New'/>">
								<img src="<c:url value='/images/submit_ticket_btn.png'/>" style="max-width:100%;"/>
							</a>
								<p> If you need help first try to get help via our group.</p>
								<p> You can find us inworld as "FeistyFelinesbreedables" </p>
								<p> Our lovely CSR's will be happy to help! </p>
						</div>
						<div class="col-md-12 layout-item">
							<h3>Auctions</h3>
							<p>Coming Soon!</p>
						</div>
					</div>
				</div>
				<div class="col-md-10 layout-item">
						<jsp:doBody/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 layout-item">
					All rights reserved
				</div>
			</div>
		</div>
	</body>
</html>
