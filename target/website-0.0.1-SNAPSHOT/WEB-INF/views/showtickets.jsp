<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@page session="true"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:layout>
<h1>Ticket Overview</h1>
	<table class="table">
		<thead>
			<tr>
				<th>Subject</th>
				<th>Status</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="ticket" items="${tickets}">
				<tr>
					<td><a href="<c:url value='/Account/Tickets/${ticket.id}'/>">${ticket.subject}</a></td>
					<td>${ticket.status}</td>
					<td>${ticket.date}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</t:layout>