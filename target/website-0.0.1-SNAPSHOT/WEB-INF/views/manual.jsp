<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<t:layout>
	<jsp:body>
	<h1>Manual</h1>
<p>Hello and thank you for your purchase!
This will be your go to guide for basic questions about our cats.
All sizes can mix with each other, which means that the Big Cats are capable of making babies with the Feisty Felines as well as the Wild cats etc.</p> 
<h2>Food</h2>
<p>
Food costs 150L$ and is available in the main store as well as via the website. <br />
Food is server-sided and as such you will need only 1 food bowl on your land to feed your cats.<br /> 
This means that you can either pay the bowl directly, or log in at the website and pay the food there. <br /> 
Each cat eats 1 unit of food per hour, so for 1 cat that means it would eat a total of 168 units per week. <br />
The 150L$ food provides 672 units, meaning that you will be able to feed 1 cat for 4 weeks.</p>
<h2>Happiness Booster</h2>
<p>
Happiness Boosters are needed to keep your cat's happiness up, and thus allowing it to gain heat.<br /> 
These Boosters cost 75L$ and are available in the main store as well via the website. <br />
This means you can either pay the happiness booster directly, or log in at the website to pay there.<br />
Each cat eats 1 unit of the happiness booster per hour, so for 1 cat that means it would eat a total of 168 units per week.<br /> 
The 75L$ Happiness Booster provides 672 units, meaning that you will be able to keep your cat happy for 4 weeks<br />
</p> 
<h2>Health Pack</h2>
<p>Health packs are available in the main store and cost 250L$. This will cure 1 cat from the sickness status to which it will continue eating and breeding (if the cat still has cycles left).<br />
To use the health pack, simply rez it out near your sick cat. This will add the health pack to your account. From here on you click on your sick cat, which will then show "heal".  Click on this option and your cat will become better.
</p>
<h2>Breeding</h2>
<p>
Once your cat reaches 5 days old it will start gaining heat. Approximately 50% heat is gained per 24 hours, so in 2 days time your cat will be ready to get pregnant. If the cat is partnered it will check if it's mate is ready too, if so then they will become pregnant. If the mate is not ready yet the cat will wait till it is ready. If the cat is not partnered it will check if there are any mates around it that are ready.  From here on it will take another 2 days for your cat to give birth and the cat will have a 1 day cool down (females only) before the cycle starts again.<br /> 
Each female has 12 cycles, which means 12 babies she can give. <br />
Each male has 18 cycles, which means 18 babies he can produce.<br />
Due to this your cats will be able to go on a 1:2 ratio of 1 male with 2 females.<br />
Please note: your cat needs to be below 70% hunger in order to gain heat, 65% percent happiness is needed in order for your cat to become pregnant.<br />
 </p>

<h2>Cat menu</h2>
<p>When you click on your Feisty Feline you will get the cat menu, here you have several options.</p>

<p>- Help Manual.<br /> 
    Clicking this option will deliver this manual to you.</p>
    
<p>- Paw Points. <br />
    Clicking on this option will ask you if you are sure you want to send your cat in for Paw Points. <br />
    This mean that your cat will be removed from the server and thus from the grid and in return you will receive a certain amount of points based on the age of your cat. <br />
    Before your action is final you will be asked if you are certain you want to do this, because once you select yes your cat cannot be retrieved ever again. <br />
    Paw Points can be saved, but as for now there are no vendors yet to exchange them for anything else, this will be introduced later on. <br /></p>
<p>- Get HUD.<br />
    This option will give you a landmark to our Headquarters, where you can pick up a HUD.</p>
    
<p>- Breeding.<br />
    The Breeding option allows you to set certain settings for your cat such as Breeding on/off, Owner, Group and Everyone.<br /> 
    The Breeding option in this menu will let you toggle breeding on and off so you could keep your cat just as a pet, or you can breed it and try to get new traits into your babies.<br />
    The other 3 options; Owner, Group and Everyone allows you to set your breeding setting for your cat for just yourself, your own group or everyone. Remember, if your cat is a male then the baby that gets born will belong to the owner of the female so the everyone option is only something you should use if you are breeding with more people without having your cats set to a specific group.
</p>
<p>- Parents.<br />
This option will deliver the information of your cat's parents to your local, unless your cat is a starter or a Limited Edition cat, in which case it does not have parents.
</p>
<p>- Traits.<br />
    The trait option will tell you in local all the traits of your cat.<br /> 
    On the top you will first find the version, name and age of your cat, followed by the following:<br /> 
    Body type:<br />
    Eye color:<br />
    Pupil:<br />
    Shape:<br />
    Ears:<br />
    Tail:</p>
<p>- Set Home. <br />
    This allows you to set the current position of your cat as it's home point. If you have movement on this means your cat will not move further away from this specific point than the amount of meters you selected.</p>
<p>- Movement.<br />
    In the movement option you can set the range for your cat to play.<br /> 
    There are a few set options;  5m, 10m, 15m, Parcel and Custom.<br />
    Parcel will let your cat play inside your own parcel and Custom will let you set a custom range.</p> 
<p>- Settings.<br />
    The settings option lets you set some of your important settings such as Hovertext, Birth, Animations and Sounds.<br />
    The hovertext option lets you toggle it on and off.<br />
    Birth will let you toggle the auto birth, which means you will have to retrieve the baby via the website if it's off. We implemented this option to make sure that your homes won't get filled up with babies in case you ever have to go for a few days.<br /> 
    Animations and Sounds lets you toggle them on and off, because while they are cute playing and making sounds, some parcels require all of these to be switched off, or it just gets too messy when you have a lot of cats.</p>
	</jsp:body>
</t:layout>