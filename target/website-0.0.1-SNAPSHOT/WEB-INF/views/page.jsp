<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<t:layout>
	<jsp:body>
		<sec:authorize access="hasAuthority('admin')">
			<c:set var="page" value ="/Admin/Edit/"/>
			<a href="<c:url value='${page}${name}' />">Edit</a>
		</sec:authorize>
		${content}	
	</jsp:body>
</t:layout>