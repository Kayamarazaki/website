<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<t:layout>
	<jsp:body>
		<sec:authorize access="hasAuthority('admin')">
			<c:set var="page" value ="/Admin/Edit/"/>
			<c:set var="slash" value ="/"/>
			<div class="col-md-12">
			<a href="<c:url value='${page}${type}${slash}${trait.name}' />">Edit</a>
			</div>
		</sec:authorize>
		<div class="col-md-5 col-md-offset-3">
			<h1>${trait.name}</h1>
			<c:set var="imagePathAfter" value = ".png"/>
			<img src="<c:url value='${imagePathBefore}${trait.name}${imagePathAfter}'/>" style="width:80%;height:auto;"/>
		</div>
		<div class="col-md-6 col-md-offset-2">
				<p><i>${trait.description}</i></p>
		</div>
	</jsp:body>
</t:layout>