<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@page session="true"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:layout>
	<jsp:body>
		<h1>Ticket history for ticket '<c:out value="${ticket.subject}"/>'</h1>
		<h2>Ticket status: '<c:out value="${ticket.status}"/>'</h2>
			<c:forEach var="ticketmessage" items="${ticketMessages}" varStatus="counter">
				<div class="col-md-12">
					<h3><c:if test="${counter.index!=0}">Re: </c:if><c:out value="${ticket.subject}"/></h3>
					<table class="table">
						<tr>
							<td class="col-md-3">Date:</td>
							<td><c:out value="${ticketmessage.date}"/></td>
						</tr>
						<tr>
							<td class="col-md-3">Posted by:</td>
							<td><c:out value="${ticketmessage.user.displayname}"/></td>
						</tr>
						<tr>
							<td class="col-md-3">Message:</td>
							<td><c:out value="${ticketmessage.message}"/></td>
						</tr>
					</table>
				</div>
			</c:forEach>
			<div class="col-md-12">
			<c:choose>
				<c:when test="${ticket.status=='Open'}">
				<form name='closeTicket' method='POST' action="<c:url value='/Account/Tickets/${ticket.id}/Close'/>">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<button type="submit" class="btn btn-default">Close Ticket</button>
				</form>
				<form name='loginForm'
		  action="<c:url value='/Account/Tickets/${ticket.id}' />" method='POST'>
					<div class="form-group">
						<label for="message">Add a new response to your ticket.</label>
						<textarea class="form-control" rows="8" name="message" placeholder="Please describe your issue in as much detail as possible" required></textarea>
					</div>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<button type="submit" class="btn btn-default">Submit</button>
				 </form>
				 </c:when>
				 <c:otherwise>
				 	<form name='closeTicket' method='POST' action="<c:url value='/Account/Tickets/${ticket.id}/Open'/>">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<button type="submit" class="btn btn-default">Open Ticket</button>
				</form>
				 </c:otherwise>
			</c:choose>
			</div>
	</jsp:body>
</t:layout>