<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@page session="true"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:layout>
	<jsp:body>
	<h1>Details of <c:out value="${cat.name}"/></h1>
	<table class="table">
		<tbody>
			<tr>
				<td>
				Age
				</td>
				<td>
				${cat.age}
				</td>
			</tr>
			<tr>
				<td>
				Sex
				</td>
				<td>
				<c:choose>
				<c:when test="${cat.male}">
				Male
				</c:when>
				<c:otherwise>
				Female
				</c:otherwise>
				</c:choose>
				</td>
			</tr>
			<tr>
				<td>
				Cycles left
				</td>
				<td>
				${cat.cyclesleft }
				</td>
			</tr>
			<tr>
				<td>
				Hunger
				</td>
				<td>
				${cat.hunger }%
				</td>
			</tr>
			<tr>
				<td>
				Happiness
				</td>
				<td>
				${cat.happiness }%
				</td>
			</tr>
			<tr>
				<td>
				Heat
				</td>
				<td>
				${cat.heat }%
				</td>
			</tr>
			<c:if test="${cat.pregnantwith != null}">
			<tr>
				<td>
				Pregnancy
				</td>
				<td>
				${cat.pregnancy }%
				</td>
			</tr>
			</c:if>
			<tr>
				<td>
				Sick
				</td>
				<td>
				<c:choose>
				<c:when test="${cat.sick}">
				Yes
				</c:when>
				<c:otherwise>
				No
				</c:otherwise>
				</c:choose>
				</td>
			</tr>
			<tr>
				<td>
				Pelt
				</td>
				<td>
				${cat.pelt.name }
				</td>
			</tr>
			<tr>
				<td>
				Pelt Color
				</td>
				<td>
				${cat.peltcolor.name }
				</td>
			</tr>
			<tr>
				<td>
				Body
				</td>
				<td>
				${cat.body.name }
				</td>
			</tr>
			<tr>
				<td>
				Ears
				</td>
				<td>
				${cat.ear.name }
				</td>
			</tr>
			<tr>
				<td>
				Eye Color
				</td>
				<td>
				${cat.eyecolor.name }
				</td>
			</tr>
			<tr>
				<td>
				Eye Shape
				</td>
				<td>
				${cat.eyeshape.name }
				</td>
			</tr>
			<tr>
				<td>
				Pupil Shape
				</td>
				<td>
				${cat.pupilshape.name }
				</td>
			</tr>
			<tr>
				<td>
				Tail
				</td>
				<td>
				${cat.tail.name }
				</td>
			</tr>
			<tr>
				<td>
				Whiskers
				</td>
				<td>
				${cat.whiskers.name }
				</td>
			</tr>
			<c:set var="path" value="/Cat/"/>
			<c:if test="${cat.father != null}">
			<tr>
				<td>
				Father
				</td>
				<td>
				<a href="<c:url value='${path}${cat.father.id }'/>">${cat.father.name }</a>
				</td>
			</tr>
			</c:if>
			<c:if test="${cat.mother != null}">
			<tr>
				<td>
				Mother
				</td>
				<td>
				<a href="<c:url value='${path}${cat.mother.id }'/>">${cat.mother.name }</a>
				</td>
			</tr>
			</c:if>
		</tbody>
	</table>
	</jsp:body>
</t:layout>