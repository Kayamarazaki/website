<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@page session="true"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:layout>
	<jsp:body>
	<h1><c:out value="${user.displayname }"/>'s Cats</h1>
	<table class="table">
		<thead>
			<tr>
				<th>Name</th>
				<th>Sex</th>
				<th>Age</th>
				<th>Details</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="cat" items="${user.owner.cats}">
				<tr>
					<td>${cat.name }</td>
					<td>
					<c:choose>
					<c:when test="${cat.male}">
						Male
					</c:when>
					<c:otherwise>
						Female	
					</c:otherwise>
					</c:choose>
					</td>
					<td>
					<c:choose>
					<c:when test="${cat.birth == null }">
						Unborn
					</c:when>
					<c:otherwise>
						${cat.age }	
					</c:otherwise>
					</c:choose>
					</td>
					<c:set var="details" value ="/Cat/"/>
					<td><a href="<c:url value='${details}${cat.id}'/>">Details</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</jsp:body>
</t:layout>