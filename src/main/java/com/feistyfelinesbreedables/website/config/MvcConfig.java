package com.feistyfelinesbreedables.website.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = { "com.feistyfelinesbreedables.website", "com.feistyfelinesbreedables.portal.entity","com.feistyfelinesbreedables.portal.service"})
@EnableJpaRepositories(basePackages = "com.feistyfelinesbreedables.website.repository", entityManagerFactoryRef="entityManagerFactory", transactionManagerRef="transactionManager")
@EnableTransactionManagement
@EnableScheduling
public class MvcConfig extends WebMvcConfigurerAdapter {
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/images/**").addResourceLocations("/WEB-INF/images/");
        registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/js/");
        registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/css/");
    }
	
		
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
    @Bean(name="multipartResolver")
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(500000000);
        return multipartResolver;
    }
    @Bean
    public InternalResourceViewResolver jspViewResolver()
    {
    	InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }
    
    
    @Bean
    public DataSource basicDataSource()
    {
    	DriverManagerDataSource source = new DriverManagerDataSource();
    	source.setDriverClassName("com.mysql.jdbc.Driver");
        source.setUrl("jdbc:mysql://localhost:3306/ffb_backend");
        source.setUsername("root");
        //Local PW:
        source.setPassword("ptZeWnNoZXZdmvjksceQ");
        //Remote PW:
        //source.setPassword("ptZeWnNoZXZdmvjksceQ");
    	
    	return source;
    }
    
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory()
    {
    	LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
    	bean.setPersistenceUnitName("persistenceUnit");
    	bean.setDataSource(basicDataSource());
    	return bean;
    }

    @Bean
    public JpaTransactionManager transactionManager()
    {
    	JpaTransactionManager bean = new JpaTransactionManager();
    	bean.setPersistenceUnitName("persistenceUnit");
    	
    	return bean;
    }
}
