package com.feistyfelinesbreedables.website.service;

import java.util.AbstractMap;
import java.util.Map.Entry;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.feistyfelinesbreedables.portal.entity.Owner;
import com.feistyfelinesbreedables.website.entity.User;
import com.feistyfelinesbreedables.website.repository.UserRepository;

@Service
@Transactional
public class UserOwnerService {
	@Autowired 
	UserRepository userRepo;
	
	
	
	public User getUser(String username) throws IndexOutOfBoundsException
	{
		User user = userRepo.findByUsername(username).get(0);
		
		return user;
	}
	
	public User getUserAndOwner(String username) throws IndexOutOfBoundsException
	{
		User user = userRepo.findByUsername(username).get(0);
		Hibernate.initialize(user.getOwner());
		
		return user;
	}
	
	public User getUserAndLoadCats(String username)
	{
		User user = userRepo.findByUsername(username).get(0);
		if(user != null && user.getOwner() != null)
		{
			Hibernate.initialize(user.getOwner());
			Hibernate.initialize(user.getOwner().getCats());
		}
		return user;
	}

	public void setDisplayName(String username, String displayname) {
		User user = userRepo.findByUsername(username).get(0);
		if(user != null)
		{
			user.setDisplayname(displayname);
			userRepo.save(user);
		}
		
	}

	public void setUserName(String username, String email) {
		User user = userRepo.findByUsername(username).get(0);
		if(user != null)
		{
			user.setUsername(email);
			userRepo.save(user);
		}
		
	}
	
	public void setPassword(String username, String password) {
		User user = userRepo.findByUsername(username).get(0);
		if(user != null)
		{
			user.setPassword(password);
			userRepo.save(user);
		}
		
	}
}
