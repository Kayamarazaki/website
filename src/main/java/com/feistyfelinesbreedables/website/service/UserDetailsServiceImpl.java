package com.feistyfelinesbreedables.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.feistyfelinesbreedables.website.entity.User;
import com.feistyfelinesbreedables.website.repository.UserRepository;
@Service
public class UserDetailsServiceImpl implements UserDetailsService
{
	@Autowired
	private UserRepository repo;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try{
			
			User user =repo.findByUsername(username).get(0);
			if(user == null)
				throw new UsernameNotFoundException("user not found");
			else
				return user;
		}catch(Exception ex)
		{
			throw new UsernameNotFoundException("user not found");
		}
		
		
	}
	
}
