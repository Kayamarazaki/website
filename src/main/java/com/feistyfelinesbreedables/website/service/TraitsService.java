package com.feistyfelinesbreedables.website.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.feistyfelinesbreedables.portal.entity.Body;
import com.feistyfelinesbreedables.portal.entity.Pelt;
import com.feistyfelinesbreedables.portal.entity.PeltColor;
import com.feistyfelinesbreedables.portal.entity.Trait;

@Service
@Transactional
public class TraitsService {
	@Autowired 
	EntityManager em;
	
	public Pelt getPeltByName(String name)
	{
		List<Pelt> list = em.createNamedQuery("getPeltByName", Pelt.class).setParameter("name", name).getResultList();
		if(list.size() == 0)
			return null;
		Pelt pelt = list.get(0);
		Hibernate.initialize(pelt.getPeltcolors());
		return pelt;
	}
	
	public PeltColor getPeltColorByName(String name)
	{
		List<PeltColor> list = em.createNamedQuery("getPeltColorByName", PeltColor.class).setParameter("name", name).getResultList();
		if(list.size() == 0)
			return null;
		PeltColor pelt = list.get(0);
		Hibernate.initialize(pelt.getPelt());
		return pelt;
	}
	
	public List<Trait> getTraits(String type)
	{
		if(type.equals("Body"))
		{
			return em.createNamedQuery("getAllBodies",Trait.class).getResultList();
		}
		else if(type.equals("Ears"))
		{
			return em.createNamedQuery("getAllEars",Trait.class).getResultList();
		}
		else if(type.equals("Eye Color"))
		{
			return em.createNamedQuery("getAllEyeColors",Trait.class).getResultList();
		}
		else if(type.equals("Eye Shape"))
		{
			return em.createNamedQuery("getAllEyeShapes",Trait.class).getResultList();
		}
		else if(type.equals("Pupil Shape"))
		{
			return em.createNamedQuery("getAllPupilShapes",Trait.class).getResultList();
		}
		else if(type.equals("Tail"))
		{
			return em.createNamedQuery("getAllTails",Trait.class).getResultList();
		}
		else if(type.equals("Whiskers"))
		{
			return em.createNamedQuery("getAllWhiskers",Trait.class).getResultList();
		}
		else
			return null;
	}

	public Trait getTraitByTypeAndName(String type, String name) {
		List<Trait> traits = null;
		if(type.equals("Body"))
		{
			traits = em.createNamedQuery("getBodyByName",Trait.class).setParameter("name", name).getResultList();
		}
		else if(type.equals("Ears"))
		{
			traits = em.createNamedQuery("getEarByName",Trait.class).setParameter("name", name).getResultList();
		}
		else if(type.equals("Eye Color"))
		{
			traits = em.createNamedQuery("getEyeColorByName",Trait.class).setParameter("name", name).getResultList();
		}
		else if(type.equals("Eye Shape"))
		{
			traits = em.createNamedQuery("getEyeShapeByName",Trait.class).setParameter("name", name).getResultList();
		}
		else if(type.equals("Pupil Shape"))
		{
			traits = em.createNamedQuery("getPupilShapeByName",Trait.class).setParameter("name", name).getResultList();
		}
		else if(type.equals("Pelt Color"))
		{
			traits = em.createNamedQuery("getPeltColorByName",Trait.class).setParameter("name", name).getResultList();
		}
		else if(type.equals("Tail"))
		{
			traits = em.createNamedQuery("getTailByName",Trait.class).setParameter("name", name).getResultList();
		}
		else if(type.equals("Whiskers"))
		{
			traits = em.createNamedQuery("getWhiskersByName",Trait.class).setParameter("name", name).getResultList();
		}
		if(traits != null && traits.size() > 0)
			return traits.get(0);
		
		
		return null;
	}

	public boolean updateTrait(String type, String name, String description, String newname) {
		Trait trait = getTraitByTypeAndName(type,name);
		if(trait != null)
		{
			trait.setDescription(description);
			trait.setName(newname);
			em.persist(trait);
			return true;
		}
		return false;
	}

	public String getPeltName(String name) {
		List<PeltColor> traits = em.createNamedQuery("getPeltColorByName",PeltColor.class).setParameter("name", name).getResultList();
		if(traits != null && traits.size() > 0)
		{
			return traits.get(0).getPelt().getName();
		}
		else
		{
			return null;
		}
		
	}
}
