package com.feistyfelinesbreedables.website.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.feistyfelinesbreedables.website.entity.Role;
import com.feistyfelinesbreedables.website.entity.StaticPage;
import com.feistyfelinesbreedables.website.entity.User;
import com.feistyfelinesbreedables.website.repository.RoleRepository;

@Service
@Transactional
public class CreationServiceWeb {
	@Autowired
	EntityManager em;
	
	@Autowired
	RoleRepository roleRepo;
	
	@Autowired
	PasswordEncoder encoder;
	
	public void createStartingData()
	{
		if(em.createNamedQuery("getUserByUsername",User.class).setParameter("username", "feistyfelinesbreedables@gmail.com").getResultList().size() < 1)
		{
			createRoles();
			createAdmin();
		}
		if(em.createNamedQuery("findPageByName",StaticPage.class).setParameter("name","Manual").getResultList().size() < 1)
		{
			createManual();
		}
		if(em.createNamedQuery("findPageByName",StaticPage.class).setParameter("name","Home").getResultList().size() < 1)
		{
			createHome();
		}
		if(em.createNamedQuery("findPageByName",StaticPage.class).setParameter("name","Lore").getResultList().size() < 1)
		{
			createLore();
		}
		
	}
	
	private void createLore() {
		StaticPage page = new StaticPage();
		page.setName("Lore");
		page.setContent("");
		em.persist(page);
		
	}

	private void createHome() {
		StaticPage page = new StaticPage();
		page.setName("Home");
		page.setContent("");
		em.persist(page);
		
	}

	private void createManual() {
		StaticPage page = new StaticPage();
		page.setName("Manual");
		page.setContent("");
		em.persist(page);
		
	}

	private void createAdmin() {
		List<Role> roles = roleRepo.findAll();
		User user = new User("feistyfelinesbreedables@gmail.com",encoder.encode("NiLi01-03"),roles);
		em.persist(user);
	}

	private void createRoles() {
		Role admin = new Role("admin");
		em.persist(admin);
		Role user = new Role("user");
		
		em.persist(user);
		
	}
}
