package com.feistyfelinesbreedables.website.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.feistyfelinesbreedables.website.entity.Ticket;
import com.feistyfelinesbreedables.website.entity.TicketMessage;
import com.feistyfelinesbreedables.website.entity.TicketStatus;
import com.feistyfelinesbreedables.website.entity.User;
import com.feistyfelinesbreedables.website.helpers.TicketMessageComparer;

@Controller
@Transactional
public class TicketController extends BaseController{

	
	
	@RequestMapping("/Account/Tickets")
	public ModelAndView showTickets(HttpServletRequest request)
	{
		List<Ticket> tickets = null;
		if(request.isUserInRole("admin"))
		{
			tickets = em.createNamedQuery("getOpenTickets",Ticket.class).getResultList();
		}
		else
		{
			User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			tickets = em.createNamedQuery("getTicketsByUser",Ticket.class).setParameter("user",user).getResultList();
		}
		ModelAndView model = new ModelAndView();
		model.addObject("tickets", tickets);
		model.addObject("active","support");
		model.setViewName("showtickets");
		model.addObject("title", "Tickets");
		
		return model;
	}
	
	@RequestMapping(value = "/Account/Tickets/New", method = RequestMethod.GET)
	public ModelAndView newTicket()
	{
		ModelAndView model = new ModelAndView();
		
		model.setViewName("newticket");
		model.addObject("active","support");
		model.addObject("title", "New Ticket");
		
		return model;
	}

	@RequestMapping(value = "/Account/Tickets/New", method = RequestMethod.POST)
	public ModelAndView createNewTicket(@RequestBody MultiValueMap<String,String> formData)
	{
		ModelAndView model = new ModelAndView();
		String subject = formData.getFirst("subject");
		String message = formData.getFirst("message");
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		Ticket ticket = new Ticket();
		TicketMessage ticketMessage = new TicketMessage();
		ArrayList<TicketMessage> ticketMessages = new ArrayList<TicketMessage>();
		ticketMessage.setUser(user);
		ticketMessage.setMessage(message);
		
		ticketMessages.add(ticketMessage);
		
		ticket.setUser(user);
		ticket.setSubject(subject);
		ticket.setTicketMessages(ticketMessages);
		ticketMessage.setTicket(ticket);
		
		em.persist(ticket);
		em.persist(ticketMessage);
		
		model.addObject("ticket", ticket);
		model.addObject("ticketMessages", ticketMessages);
		model.addObject("title", "Tickets");
		model.addObject("active","support");
		model.setViewName("showticket");
		
		return model;
	}
	
	@RequestMapping(value= "/Account/Tickets/{id:[\\d]+}/{action}", method = RequestMethod.POST)
	public ModelAndView respondToTicket(@PathVariable("id")long id, HttpServletRequest request, @PathVariable("action") String action)
	{
		ModelAndView model = new ModelAndView();
		try
		{
			Ticket ticket = em.createNamedQuery("getTicketById",Ticket.class).setParameter("id",id).getResultList().get(0);
			User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			
			if(!ticket.getUser().equals(user) && !request.isUserInRole("admin"))
			{
				return show403();
			}
			else
			{
				if(action.equals("Open"))
				{
					ticket.setStatus(TicketStatus.Open);
				}
				else
				{
					ticket.setStatus(TicketStatus.Closed);
				}
				em.persist(ticket);
				List<TicketMessage> ticketMessages= ticket.getTicketMessages();
				ticketMessages.sort(new TicketMessageComparer());
				model.addObject("ticket", ticket);
				model.addObject("ticketMessages", ticketMessages);
				model.addObject("title", "Tickets");
				model.addObject("active","support");
				model.setViewName("showticket");
				
			}
		}
		catch(Exception e)
		{
			return show404();
		}
		return model;
	}
	
	@RequestMapping(value= "/Account/Tickets/{id:[\\d]+}", method = RequestMethod.POST)
	public ModelAndView respondToTicket(@PathVariable("id")long id, @RequestBody MultiValueMap<String,String> formData, HttpServletRequest request)
	{
		
		ModelAndView model = new ModelAndView();
		try
		{
			Ticket ticket = em.createNamedQuery("getTicketById",Ticket.class).setParameter("id",id).getResultList().get(0);
			User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			
			if(!ticket.getUser().equals(user) && !request.isUserInRole("admin"))
			{
				return show403();
			}
			else
			{
				String message = formData.getFirst("message");
							
				TicketMessage ticketMessage = new TicketMessage();
				
				ticketMessage.setMessage(message);
				ticketMessage.setTicket(ticket);
				ticketMessage.setUser(user);
				em.persist(ticketMessage);
				List<TicketMessage> ticketMessages= ticket.getTicketMessages();
				ticketMessages.sort(new TicketMessageComparer());
				model.addObject("ticket", ticket);
				model.addObject("ticketMessages", ticketMessages);
				model.addObject("title", "Tickets");
				model.addObject("active","support");
				model.setViewName("showticket");
			}
		}
		catch(Exception e)
		{
			return show404();
		}
		return model;
	}
	
	@RequestMapping(value= "/Account/Tickets/{id:[\\d]+}")
	public ModelAndView showTicket(@PathVariable("id")long id, HttpServletRequest request)
	{
		ModelAndView model = new ModelAndView();
		try
		{
			Ticket ticket = em.createNamedQuery("getTicketById",Ticket.class).setParameter("id",id).getResultList().get(0);
			User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if(!ticket.getUser().equals(user) && !request.isUserInRole("admin"))
			{
				return show403();
			}
			else
			{
				List<TicketMessage> ticketMessages= ticket.getTicketMessages();
				ticketMessages.sort(new TicketMessageComparer());
				model.addObject("ticket", ticket);
				model.addObject("ticketMessages", ticketMessages);
				model.addObject("title", "Tickets");
				model.addObject("active","support");
				model.setViewName("showticket");
			}
		}
		catch(Exception e)
		{
			return show404();
		}
		return model;
	}
}
