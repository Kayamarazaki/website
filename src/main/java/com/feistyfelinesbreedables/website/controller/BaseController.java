package com.feistyfelinesbreedables.website.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.feistyfelinesbreedables.portal.entity.Pelt;
import com.feistyfelinesbreedables.website.entity.StaticPage;

public class BaseController {
	@Autowired
	protected EntityManager em;
	
	@ModelAttribute("peltNames")
	protected String[] peltNames()
	{
		return getPeltNames();
	}
	
	
	private String[] peltNames;
	
	private Timestamp lastcheck;
	
	private String[] getPeltNames()
	{
		if(lastcheck == null || System.currentTimeMillis() - lastcheck.getTime() > (15*60*1000))
		{
			refresh();
		}
		return peltNames;
	}
	
	private void refresh()
	{
		TypedQuery<Pelt> query = em.createNamedQuery("getAllPelts", Pelt.class);
		List<Pelt> list = query.getResultList();
		
		peltNames = new String[list.size()];
		int i = 0;
		for(Pelt pelt : list)
		{
			peltNames[i] = pelt.getName();
			i++;
		}
	}
	
	
	protected ModelAndView page(String name)
	{
		ModelAndView model = new ModelAndView();
		List<StaticPage> page = em.createNamedQuery("findPageByName", StaticPage.class).setParameter("name", name).getResultList();
		if(page.size() > 0)
		{
			model.addObject("active", name.toLowerCase().replace(" ", ""));
			model.addObject("content",page.get(0).getContent());
			model.addObject("title", name);
			model.addObject("name", name);
			model.setViewName("page");
		}
		else
		{
			return show404();
		}
		return model;
	}
	
	protected ModelAndView show404()
	{
		ModelAndView model = new ModelAndView();
		model.setViewName("404");
		model.addObject("title", 404);
		return model;
	}
	protected ModelAndView show403()
	{
		ModelAndView model = new ModelAndView();
		model.setViewName("403");
		model.addObject("title", 403);
		return model;
	}
}
