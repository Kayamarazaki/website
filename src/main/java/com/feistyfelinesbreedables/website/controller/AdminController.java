package com.feistyfelinesbreedables.website.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.feistyfelinesbreedables.portal.entity.Trait;
import com.feistyfelinesbreedables.website.entity.StaticPage;
import com.feistyfelinesbreedables.website.service.TraitsService;

@Controller
@Transactional
@RequestMapping("/Admin")
public class AdminController extends BaseController{
	@Autowired
	private TraitsService traitsService;
	
	@RequestMapping(value = "/Edit/{type}/{name}", method = RequestMethod.GET)
	public ModelAndView editTraitView(@PathVariable(value="name") String name, @PathVariable(value="type") String type)
	{
		ModelAndView model = new ModelAndView();
		Trait trait = traitsService.getTraitByTypeAndName(type, name);
		
		if(trait != null)
		{
			model.addObject("trait", trait);
			model.addObject("title", "Edit trait " + name);
			model.addObject("type",type);
			model.setViewName("editTrait");
		}
		else
		{
			model = show404();
		}
		
		return model;
	}
	
	
	@RequestMapping(value = "/Edit/{type}/{name}", method = RequestMethod.POST)
	public ModelAndView editTrait(@PathVariable(value="name") String name, @PathVariable(value="type") String type, @RequestParam MultiValueMap<String,String> map, @RequestParam("file") MultipartFile file, HttpServletRequest request)
	{
		ModelAndView model = new ModelAndView();
		String description = map.getFirst("description");
		String newname = map.getFirst("newname");
		if(traitsService.updateTrait(type,name,description,newname))
		{
			model.setViewName("redirect:/Traits/" + type + "/Details/" + newname);
			if(type.equals("Pelt Color"))
			{
				type = type + "/" + traitsService.getPeltName(newname);
			}
			if(file != null && !file.isEmpty())
			{
				Timestamp time = new Timestamp(System.currentTimeMillis());
                File fileNew=new File(request.getSession().
                		getServletContext().getRealPath("/WEB-INF/images/" + type), newname + ".png");
                File directoryNew=new File(request.getSession().
                		getServletContext().getRealPath("/WEB-INF/images/" + type));
                
                
                byte[] bytes;
				try {
					if(!directoryNew.exists())
						directoryNew.mkdirs();
					bytes = file.getBytes();
	                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileNew));

	                stream.write(bytes);
	                stream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (!name.equals(newname))
			{
				File fileOld=new File(request.getSession().
                		getServletContext().getRealPath("/WEB-INF/images/" + type), name + ".png");
				File fileNew=new File(request.getSession().
                		getServletContext().getRealPath("/WEB-INF/images/" + type), newname + ".png");
				if(fileOld.exists())
				{
					fileNew.delete();
					fileOld.renameTo(fileNew);
				}
			}
			
		}
		else
		{
			model = show404();
		}
		return model;
	}
	
	
	@RequestMapping(value = "/Edit/{name}", method = RequestMethod.GET)
	public ModelAndView editView(@PathVariable(value="name") String name)
	{
		ModelAndView model = new ModelAndView();
		List<StaticPage> list = em.createNamedQuery("findPageByName",StaticPage.class).setParameter("name",name).getResultList();
		if(list.size() > 0)
		{
			model.addObject("content",list.get(0).getContent());
			model.addObject("active", name.toLowerCase().replace(" ", ""));
			model.addObject("name", name);
			model.addObject("title", "Editing Page: " + name);
			model.setViewName("edit");
		}
		else
		{
			return show404();
		}
		
		return model;
	}
	
	@RequestMapping(value="/Images", method=RequestMethod.POST)
	public ModelAndView images(MultipartHttpServletRequest request)
	{
		ModelAndView model = new ModelAndView();
		Iterator<String> itr =  request.getFileNames();

	     String fileName=itr.next();
	     MultipartFile uploaded = request.getFile(fileName);

	     if (!uploaded.isEmpty()) {
	            try {
	            	Timestamp time = new Timestamp(System.currentTimeMillis());
	                File fileNew=new File(request.getSession().
	                		getServletContext().getRealPath("/WEB-INF/images/"), uploaded.getOriginalFilename());


	                byte[] bytes = uploaded.getBytes();
	                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileNew));

	                stream.write(bytes);
	                stream.close();

	                model.addObject("info", "Upload succesful!");
	            } catch (Exception e) {
	                model.addObject("error", "Upload Failed.");
	            }
	        } else {
	        	model.addObject("error", "Upload Failed.");
	        }
		

		String imagepath = request.getSession().getServletContext().getRealPath("/WEB-INF/images");
		
		File[] files = new File(imagepath).listFiles();
		List<String> list = new ArrayList<String>();
		for(File file: files)
		{
			String path = file.getPath();
			if(path.indexOf("/images") < 0)
			{
				list.add(path.substring(path.indexOf("\\images\\") + "\\images\\".length()));
			}
			else
				list.add(path.substring(path.indexOf("/images/") + "/images/".length()));
		}
		model.addObject("images",list);
		model.addObject("active","account");
		model.setViewName("images");
		
		
		return model;
	}
	
	@RequestMapping(value="/Images", method=RequestMethod.GET)
	public ModelAndView images(HttpServletRequest request)
	{
		ModelAndView model = new ModelAndView();
		String imagepath = request.getSession().getServletContext().getRealPath("/WEB-INF/images");
		
		File[] files = new File(imagepath).listFiles();
		List<String> list = new ArrayList<String>();
		for(File file: files)
		{
			String path = file.getPath();
			if(path.toLowerCase().endsWith(".png"))
			{
				if(path.indexOf("/images") < 0)
				{
					list.add(path.substring(path.indexOf("\\images\\") + "\\images\\".length()));
				}
				else
					list.add(path.substring(path.indexOf("/images/") + "/images/".length()));
			}
		}
		model.addObject("images",list);
		model.addObject("active","account");
		model.setViewName("images");
		
		
		return model;
	}
	
	@RequestMapping(value = "/Edit/{name}", method = RequestMethod.POST)
	public ModelAndView edit(@RequestBody MultiValueMap<String,String> map)
	{
		String originalName = map.getFirst("originalname");
		List<StaticPage> list = em.createNamedQuery("findPageByName",StaticPage.class).setParameter("name",originalName).getResultList();
		if(list.size() > 0)
		{
			StaticPage page = list.get(0);
			
			page.setName(map.getFirst("name"));
			page.setContent(map.getFirst("content"));
			em.persist(page);
			
			return page(map.getFirst("name"));
		}
		else
		{
			return show404();
		}
	}
	
}
