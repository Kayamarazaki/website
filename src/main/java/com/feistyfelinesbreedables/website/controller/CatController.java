package com.feistyfelinesbreedables.website.controller;

import java.security.Principal;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.feistyfelinesbreedables.portal.entity.Cat;
import com.feistyfelinesbreedables.portal.entity.Owner;
import com.feistyfelinesbreedables.portal.service.CatService;
import com.feistyfelinesbreedables.website.entity.Role;
import com.feistyfelinesbreedables.website.entity.User;
import com.feistyfelinesbreedables.website.repository.RoleRepository;
import com.feistyfelinesbreedables.website.repository.UserRepository;
import com.feistyfelinesbreedables.website.service.UserOwnerService;

@Controller
@RequestMapping("/Cat")
public class CatController extends BaseController {
	@Autowired
	private CatService catService;
	
	@RequestMapping("/{id}")
	public ModelAndView details(@PathVariable(value="id") long id)
	{
		ModelAndView model = new ModelAndView();
		Cat cat = catService.getCatDetails(id);
		if(cat != null)
		{
			model.addObject("cat",cat);
			model.setViewName("cat");
			model.addObject("title", "Details");
		}
		else
		{
			model = show404();
		}
		
		return model;
	}
}
