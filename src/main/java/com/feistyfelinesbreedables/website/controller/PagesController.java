package com.feistyfelinesbreedables.website.controller;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Transactional
@RequestMapping("/Pages")
public class PagesController extends BaseController {

	@RequestMapping("/Manual")
	public ModelAndView manual() 
	{
		return page("Manual");
	}

	@RequestMapping("/Lore")
	public ModelAndView lore() 
	{
		return page("Lore");
	}
}
