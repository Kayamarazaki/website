package com.feistyfelinesbreedables.website.controller;

import java.security.Principal;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.feistyfelinesbreedables.portal.entity.Owner;
import com.feistyfelinesbreedables.website.entity.Role;
import com.feistyfelinesbreedables.website.entity.User;
import com.feistyfelinesbreedables.website.repository.RoleRepository;
import com.feistyfelinesbreedables.website.repository.UserRepository;
import com.feistyfelinesbreedables.website.service.UserOwnerService;

@Controller
@RequestMapping("/Account")
public class AccountController extends BaseController {
	@Autowired 
	UserOwnerService userOwnerService;
	
	@Autowired
	PasswordEncoder encoder;
	
	@RequestMapping(path="/Change/{type}", method=RequestMethod.POST)
	public ModelAndView changePost(Principal principal, @PathVariable("type") String type, @RequestBody MultiValueMap<String,String> formData)
	{
		ModelAndView model = new ModelAndView();
		if(principal != null)
		{
			if(type.equals("Display Name"))
			{
				String displayname = formData.getFirst("displayname");
				if(displayname != null)
				{
					userOwnerService.setDisplayName(principal.getName(),displayname);
					model.setViewName("redirect:/Account/Profile");
				}
				else
				{
					model.setViewName("change/display");
					model.addObject("current","");
					model.addObject("error","Please provide a display name.");
				}
			}
			else if (type.equals("Email"))
			{
				String email = formData.getFirst("email");
				if(email != null)
				{
					userOwnerService.setUserName(principal.getName(),email);
					model.setViewName("redirect:/Account/Profile");
					
				}
				else
				{
					model.setViewName("change/email");
					model.addObject("error","Please provide an e-mail address.");
					model.addObject("current","");
				}
			}
			else if (type.equals("Password"))
			{
				String current = formData.getFirst("current");
				String replace = formData.getFirst("replace"); 
				String repeat = formData.getFirst("repeat");
				User user = userOwnerService.getUser(principal.getName());
				if(user != null)
				{
					if(current != null && replace != null && repeat != null)
					{
						if(replace.equals(repeat))
						{
							if(encoder.matches(current, user.getPassword()))
							{
								userOwnerService.setPassword(principal.getName(),replace);
								model.setViewName("redirect:/Account/Profile");
							}
							else
							{
								model.setViewName("change/password");
								model.addObject("error","Your current password was not correct.");
							}
						}
						else
						{
							model.setViewName("change/password");
							model.addObject("error","Your new password doesn't match.");
						}
					}
					else
					{
						model.setViewName("change/password");
						model.addObject("error","Please fill out all fields.");
					}
				}
				else
				{
					model.setViewName("home");
				}
			}
			else
			{
				model = show404();
			}
		}
		else
		{
			model.setViewName("home");
		}
		
		
		return model;
	}
	
	@RequestMapping(path="/Change/{type}", method=RequestMethod.GET)
	public ModelAndView change(Principal principal, @PathVariable("type") String type)
	{
		ModelAndView model = new ModelAndView();
		if(principal != null)
		{
			if(type.equals("Display Name"))
			{
				model.setViewName("change/display");
				User user = userOwnerService.getUser(principal.getName());
				model.addObject("current",user.getDisplayname());
			}
			else if (type.equals("Email"))
			{
				model.setViewName("change/email");
				User user = userOwnerService.getUser(principal.getName());
				model.addObject("current",user.getUsername());
			}
			else if (type.equals("Password"))
			{
				model.setViewName("change/password");
			}
			else
			{
				model = show404();
			}
		}
		else
		{
			model.setViewName("home");
		}
		
		
		return model;
	}
	
	@RequestMapping("/Profile")
	public ModelAndView profile(Principal principal)
	{
		ModelAndView model = new ModelAndView();
		if(principal != null)
		{
			User user = userOwnerService.getUserAndOwner(principal.getName());
			int food = user.getOwner().getFood();
			int happiness = user.getOwner().getHappiness();
			int healthpacks = user.getOwner().getHealthpack();
			int pawpoints = user.getOwner().getPawpoints();
			int petPotions = user.getOwner().getPetpotions();
			//TODO: Implement hibernateBaskets
			int hibernateBaskets = 0;
			model.addObject("active", "account");
			model.addObject("user", user);
			model.addObject("pawpoints",pawpoints);
			model.addObject("food",food);
			model.addObject("healthpacks",healthpacks);
			model.addObject("happiness",happiness);
			model.addObject("petPotions",petPotions);
			model.addObject("hibernateBaskets",hibernateBaskets);
			model.addObject("title", "Profile");
			model.setViewName("profile");
		}
		else
		{
			model.setViewName("home");
		}
		
		
		return model;
	}
	
	@RequestMapping("/Cats")
	public ModelAndView cats(Principal principal)
	{
		ModelAndView model = new ModelAndView();
		int food = 0;
		int happiness = 0;
		int healthpacks = 0;
		int pawpoints = 0;
		if(principal != null)
		{
			User user = userOwnerService.getUserAndLoadCats(principal.getName());
			model.addObject("user", user);
			model.addObject("active", "account");
			model.addObject("title", "Cats");
			model.setViewName("cats");
		}
		else
		{
			model.setViewName("home");
		}
		
		
		return model;
	}
}