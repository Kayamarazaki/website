package com.feistyfelinesbreedables.website.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.feistyfelinesbreedables.portal.entity.Cat;
import com.feistyfelinesbreedables.portal.entity.CatType;
import com.feistyfelinesbreedables.website.entity.StaticPage;

@Controller
@Transactional
public class HomeController extends BaseController {
	@RequestMapping(value={"","/","/Home"})
	public ModelAndView home() 
	{
		return page("Home");
	}
}
