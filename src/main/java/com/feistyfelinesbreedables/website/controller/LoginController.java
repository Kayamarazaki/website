package com.feistyfelinesbreedables.website.controller;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.feistyfelinesbreedables.portal.entity.Owner;
import com.feistyfelinesbreedables.website.entity.Role;
import com.feistyfelinesbreedables.website.entity.User;
import com.feistyfelinesbreedables.website.repository.RoleRepository;
import com.feistyfelinesbreedables.website.repository.UserRepository;

@Controller
public class LoginController extends BaseController{
	@Autowired
	RoleRepository roleRepo;
	
	@Autowired
	UserRepository userRepo;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,@RequestParam(value = "logout", required = false) String logout, Principal principal) 
	{
		ModelAndView model = new ModelAndView();
		if(principal != null)
		{
			return page("Home");
		}
	    
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}
		
		if (logout != null) {
			  model.addObject("msg", "You've been logged out successfully.");
		}
		model.addObject("active","login");
		model.setViewName("login");
		model.addObject("title", "Login");
		  return model;

	}

	@RequestMapping(value= "/register", method = RequestMethod.POST)
	public ModelAndView register(@RequestBody MultiValueMap<String,String> formData, Principal principal) 
	{
		ModelAndView model = new ModelAndView();
		if(principal != null)
		{
			model.setViewName("home");
			return model;
		}
		String avatarKey = formData.getFirst("avatar");
		String key = formData.getFirst("key");
		try
		{
			UUID.fromString(avatarKey);
			if(!key.isEmpty() && ! avatarKey.isEmpty() && encoder.matches(avatarKey, key))
			{
				String username = formData.getFirst("username");
				String password = formData.getFirst("password");
				String displayname = formData.getFirst("displayname");
				String confirmPassword = formData.getFirst("confirmPassword");
				if(password == null || confirmPassword == null || password.isEmpty() || confirmPassword.isEmpty() || username == null || username.isEmpty())
				{
					model.addObject("error", "Please fill in all fields.");
					model.addObject("avatar",avatarKey);
					model.addObject("key",key);
					model.setViewName("register");
				}
				else
				{
					if(password.equals(confirmPassword))
					{
						Owner o = Owner.getOrCreateOwner(avatarKey, em);
						if(userRepo.findByOwner(o).size() <1)
						{
							List<Role> roles = roleRepo.findByAuthority("user");
							User user = new User(username,encoder.encode(password),roles);
							user.setOwner(o);
							user.setDisplayname(displayname);
							userRepo.save(user);
							
							model.addObject("msg","Your account has succesfully been registered! You can now log in!");
							model.addObject("title", "Login");
							model.setViewName("login");
						}
						else
						{
							model.addObject("error", "An account has already been registered for this avatar.");
							model.addObject("avatar",avatarKey);
							model.addObject("key",key);
							model.addObject("title", "Register");
							model.setViewName("register");
						}
						
					}
					else
					{
						model.addObject("error", "Passwords do not match.");
						model.addObject("avatar",avatarKey);
						model.addObject("key",key);
						model.setViewName("register");
					}
				}
				
			}
			else
			{
				return show403();
			}
		}catch (Exception ex)
		{
			return show403();
		}
		return model;
	}
	
	@Autowired
	PasswordEncoder encoder;
	
	@RequestMapping(value= "/register", method = RequestMethod.GET)
	public ModelAndView register(@RequestParam(value = "avatar", required = true) String avatarKey, @RequestParam(value = "key", required = true) String key, Principal principal)
	{
		ModelAndView model = new ModelAndView();
		if(principal != null)
		{
			return page("Home");
		}
		try
		{
			UUID.fromString(avatarKey);
			if(!key.isEmpty() && ! avatarKey.isEmpty() && encoder.matches(avatarKey, key))
			{
				model.addObject("avatar",avatarKey);
				model.addObject("key",key);
				model.setViewName("register");
				model.addObject("title", "Register");
			}
			else
			{
				return show403();
			}
		}catch (Exception ex)
		{
			return show403();
		}
		return model;
	}
}
