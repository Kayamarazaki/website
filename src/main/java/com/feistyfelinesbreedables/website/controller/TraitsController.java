package com.feistyfelinesbreedables.website.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.feistyfelinesbreedables.portal.entity.Pelt;
import com.feistyfelinesbreedables.portal.entity.PeltColor;
import com.feistyfelinesbreedables.portal.entity.Trait;
import com.feistyfelinesbreedables.website.service.TraitsService;

@Controller
@Transactional
@RequestMapping("/Traits")
public class TraitsController extends BaseController {
	
	@Autowired
	private TraitsService traitsService;
	
	@RequestMapping("/Pelt Color/{name}")
	public ModelAndView showPelts(@PathVariable(value="name") String name)
	{
		ModelAndView model = new ModelAndView();
		Pelt pelt = traitsService.getPeltByName(name);
		
		if(pelt == null)
			model = show404();
		else
		{
			
			model.addObject("active","pelts");
			model.addObject("pelt",pelt);
			model.addObject("title",pelt.getName());
			model.setViewName("pelts");
		}
		return model;
		
	}
	
	@RequestMapping("/Pelt Color/Details/{name}")
	public ModelAndView showPeltDetails(@PathVariable(value="name") String name)
	{
		ModelAndView model = new ModelAndView();
		PeltColor pelt = traitsService.getPeltColorByName(name);
		
		if(pelt == null)
			model = show404();
		else
		{
			model.addObject("active","pelts");
			model.addObject("peltcolor",pelt);
			model.addObject("title",pelt.getName());
			model.setViewName("peltcolor");
		}
		return model;
	}
	
	@RequestMapping("/{type}")
	public ModelAndView showTraits(@PathVariable(value="type") String type)
	{
		ModelAndView model = new ModelAndView();
		List<Trait> traits = traitsService.getTraits(type);
		if(traits != null && traits.size() > 0)
		{
			model.addObject("title", type);
			model.addObject("urlPath", "/Traits/" + type + "/Details/");
			model.addObject("imagePathBefore", "/images/" + type + "/");
			model.addObject("traits",traits);
			model.setViewName("traits");
		}
		else
		{
			model = show404();
		}
		
		
		return model;
	}
	
	@RequestMapping("/{type}/Details/{name}")
	public ModelAndView showTraitDetails(@PathVariable(value="type") String type, @PathVariable(value="name") String name)
	{
		ModelAndView model = new ModelAndView();
		
		Trait trait = traitsService.getTraitByTypeAndName(type,name);
		if(trait != null)
		{
			model.addObject("title",trait.getName());
			model.addObject("type",type);
			model.addObject("trait", trait);
			model.addObject("urlPath", "/Traits/" + type + "/");
			model.addObject("imagePathBefore", "/images/" + type + "/");
			model.setViewName("trait");
		}
		
		return model;
	}
	
}
