package com.feistyfelinesbreedables.website.helpers;

import java.util.Comparator;

import com.feistyfelinesbreedables.website.entity.TicketMessage;

public class TicketMessageComparer implements Comparator<TicketMessage>
{

	@Override
	public int compare(TicketMessage arg0, TicketMessage arg1) {
		long time0 = arg0.getDate().getTime();
		long time1= arg0.getDate().getTime();
		
		if(time0 == time1)
		{
			return 0;
		}
		else if(time0 < time1)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
	
}