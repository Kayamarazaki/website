package com.feistyfelinesbreedables.website.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.feistyfelinesbreedables.portal.entity.Owner;
import com.feistyfelinesbreedables.website.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {
    void delete(User deleted);
    
    List<User> findAll();
 
    User findOne(Long id);
 
    User save(User persisted);
    
    List<User> findByUsername(String username);
    
    List<User> findByOwner(Owner owner);
}
