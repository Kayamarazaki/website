package com.feistyfelinesbreedables.website.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.feistyfelinesbreedables.website.entity.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
    void delete(Role deleted);
    
    List<Role> findAll();
 
    Role findOne(Long id);
 
    Role save(Role persisted);
    
    List<Role> findByAuthority(String authority);
}