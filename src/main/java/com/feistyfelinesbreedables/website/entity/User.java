package com.feistyfelinesbreedables.website.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.feistyfelinesbreedables.portal.entity.Owner;

@Entity
@Table(name="user")
@NamedQueries({
	@NamedQuery(name="getUserByUsername", query="SELECT u FROM User u WHERE u.username = :username")
})
public class User implements UserDetails{
	public User(String username, String password, List<Role> roles)
	{
		this.roles = roles;
		this.username = username;
		this.password = password;
		this.active = true;
	}
	
	protected User()
	{
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional=false)
	private long id;
	
	@Basic(optional=false)
	private String username;
	
	public void setUsername(String username) {
		this.username = username;
	}

	@Basic(optional=false)
	private String password;
	
	public void setPassword(String password) {
		this.password = password;
	}

	@Basic(optional=false)
	private boolean active;
	
	private String displayname;
	
	@OneToMany(mappedBy="user")
	private List<Ticket> tickets;
	
	@ManyToMany(fetch= FetchType.EAGER)
	@JoinTable(name="user_role", joinColumns= {@JoinColumn(name = "user_id", referencedColumnName="id")}, inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id")})
	private List<Role> roles;
	
	@OneToOne(optional=true)
	@JoinColumn(name="owner_id", unique = true, nullable = true, updatable = true)
	private Owner owner;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return active;
	}

	@Override
	public boolean isAccountNonLocked() {
		return active;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return active;
	}
	
	public void setOwner(Owner o)
	{
		this.owner = o;
	}
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof User && ((User)o).getId() == this.getId())
		{
			return true;
		}
		return false;
		
	}

	public long getId() {
		return id;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public Owner getOwner() {
		return owner;
	}
	
	
	
}
