package com.feistyfelinesbreedables.website.entity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "role")
public class Role implements GrantedAuthority {
	public Role(String authority)
	{
		this.authority = authority;
	}
	protected Role()
	{
		
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional=false)
	private long id;
	
	@Basic(optional=false)
	private String authority;
	
	@Override
	public String getAuthority() {
			
		return authority;
	}

}
