package com.feistyfelinesbreedables.website.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="ticket")
@NamedQueries({
	@NamedQuery(name="getTicketsByUser", query = "SELECT t FROM Ticket t WHERE t.user = :user"),
	@NamedQuery(name="getTicketById", query = "SELECT t FROM Ticket t WHERE t.id = :id"),
	@NamedQuery(name="getOpenTickets", query="SELECT t FROM Ticket t WHERE t.status = com.feistyfelinesbreedables.website.entity.TicketStatus.Open")
}
)
public class Ticket {
	public Ticket()
	{
		this.status = TicketStatus.Open;
		this.date = new Timestamp(System.currentTimeMillis());
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional=false)
	private long id;
	
	@Enumerated(EnumType.ORDINAL)
	private TicketStatus status;
	
	private Timestamp date;
	
	private String subject;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="user_id")
	private User user;
	
	@OneToMany(mappedBy="ticket")
	private List<TicketMessage> ticketMessages;

	public List<TicketMessage> getTicketMessages() {
		return ticketMessages;
	}

	public User getUser() {
		return user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setTicketMessages(List<TicketMessage> ticketMessages) {
		this.ticketMessages = ticketMessages;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}
	
	
	
}
