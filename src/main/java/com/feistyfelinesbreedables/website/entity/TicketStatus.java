package com.feistyfelinesbreedables.website.entity;

public enum TicketStatus
{
	Open(0),
	Closed(1);
	
	private final int value;
    private TicketStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}