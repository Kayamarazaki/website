<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@page session="true"%>
<t:layout>
	<jsp:body>
	<h1>Feisty Felines Breedables</h1>
	<p>Feisty Felines Breedables is a new cat breedable in Second Life.<br /> 
	You can find us here: </p>
	<a href=""><img src="http://www.feistyfelinesbreedables.com:8080/website/images/headquarters.png" style="max-width:100%;"></a>
	<p> 
We have 3 different types of cats:
<ul>
<li>Big Cats</li>
<li>Wild Cats</li>
<li>Feisty Felines</li>
</ul>
</p>
<h2>Lore</h2>
<p>
Long before time, the heavens were occupied by the Gods and Goddesses of old. Among them were the lovely Bastet, Goddess of Protection, and her husband Anubis, God of the afterlife. Then one day, war broke out between them Ã???Ã??Ã?Â¢??war that threatened life on earth as we know it.</p>
<p>Fearing for the safety of humanity, Bastet was determined to protect her people and contemplating how, knowing she was forbidden to leave the heavens. Then one day, she had it. She would send her followers in various forms that would serve to protect life until the end of time and preserve their safety.</p> 
<p>The time came, and she sent her most devoted followers to earth. They took the forms of four-legged beasts. Some were larger in size and more feral than others, choosing to protect nature while others adapted smaller forms, choosing to become companions to humans.</p>
<p>Soon, the war ended and Bastet gave her followers the option to return to her. Some chose to return while others remained on earth and became what we now know as felines. They walk among us to this day, still protecting the beings they have come to love.</p>
</p>
<p>Please note This website is still under construction.</p>
	</jsp:body>
</t:layout>