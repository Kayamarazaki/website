<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@page session="true"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:layout>
	<c:if test="${not empty error}">
		<p class="bg-danger">${error}</p>
	</c:if>

	<form name='RegisterForm'
	  action="<c:url value='/register' />" method='POST'>
	  	<div class="form-group">
			<label for="loginEmail">Email address</label>
			<input type="email" class="form-control" name="username" placeholder="Email" required>
		</div>
		<div class="form-group">
			<label for="loginEmail">Display Name</label>
			<input type="text" class="form-control" name="displayname" placeholder="Display Name" required>
		</div>
		<div class="form-group">
			<label for="loginPassword">Password</label>
			<input type="password" class="form-control" name="password" placeholder="Password" required>
		</div>
		<div class="form-group">
			<label for="loginPassword">Password</label>
			<input type="password" class="form-control" name="confirmPassword" placeholder="Password" required>
		</div>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<input type="hidden" name="avatar" value="${avatar}"/>
		<input type="hidden" name="key" value="${key}"/>
		<button type="submit" class="btn btn-default">Register</button>
	</form>
</t:layout>