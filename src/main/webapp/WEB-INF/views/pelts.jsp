<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<t:layout>
	<jsp:body>
		<h1>${pelt.name}</h1>
		<c:set var="urlPath" value ="/Traits/Pelt Color/Details/"/>
		<c:set var="imagePathBefore" value ="/images/Pelt Color/"/>
		<c:set var="slash" value="/"/>
		<c:set var="imagePathAfter" value = ".png"/>
		<c:forEach var="peltcolor" items="${pelt.peltcolors}" varStatus="counter">
			<c:if test="${!peltcolor.hidden}">
			<a href="<c:url value='${urlPath}${peltcolor.name}'/>">
			<div class="col-md-4" style="text-align:center;margin:auto;">
				<img src="<c:url value='${imagePathBefore}${pelt.name}${slash}${peltcolor.name}${imagePathAfter}'/>" style="width:80%;"/>
				<p><b>${peltcolor.name }</b></p>
			</div>
			</a>
			</c:if>
		</c:forEach>	
	</jsp:body>
</t:layout>