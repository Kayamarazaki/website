<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<t:layout>
	<jsp:body>
		<h1>${title}</h1>
		<c:set var="slash" value="/"/>
		<c:set var="imagePathAfter" value = ".png"/>
		<c:forEach var="trait" items="${traits}" varStatus="counter">
			<c:if test="${!trait.hidden}">
			<a href="<c:url value='${urlPath}${trait.name}'/>">
			<div class="col-md-4" style="text-align:center;margin:auto;">
				<img src="<c:url value='${imagePathBefore}${trait.name}${imagePathAfter}'/>" style="width:80%;"/>
				<p><b>${trait.name }</b></p>
			</div>
			</a>
			</c:if>
		</c:forEach>	
	</jsp:body>
</t:layout>