<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@page session="true"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:layout>
	<jsp:body>
		<h3>Welcome <c:out value="${user.displayname}"/></h3>
		<table class="table" style="width:100%">
			<tr>
				<td class="col-md-3"><b>E-mail address:</b></td>
				<td><c:out value="${user.username}"/></td>
				<td></td>
			</tr>
			<tr>
				<td class="col-md-3"><b>Display Name:</b></td>
				<td><c:out value="${user.displayname}"/></td>
				<td></td>
			</tr>
			<tr>
				<td class="col-md-3"><b>Password:</b></td>
				<td>******</td>
				<td><a href="<c:url value='/Account/Change/Password'/>">change</a>--></td>
			</tr>
			<tr>
				<td class="col-md-3"><b>Pawpoints:</b></td>
				<td><c:out value="${pawpoints}"/></td>
				<td></td>
			</tr>
			<tr>
				<td class="col-md-3"><b>Health Packs:</b></td>
				<td><c:out value="${healthpacks}"/></td>
				<td></td>
			</tr>
			<tr>
				<td class="col-md-3"><b>Food:</b></td>
				<td><c:out value="${food}"/></td>
				<td><!--<a href="#">add</a>--></td>
			</tr>
			<tr>
				<td class="col-md-3"><b>Happiness Booster:</b></td>
				<td><c:out value="${happiness}"/></td>
				<td><!--<a href="#">add</a>--></td>
			</tr>
			<tr>
				<td class="col-md-3"><b>Forever Pet Potion:</b></td>
				<td><c:out value="${petPotions}"/></td>
				<td></td>
			</tr>
			<tr>
				<td class="col-md-3"><b>Hibernate Basket:</b></td>
				<td><c:out value="${hibernateBaskets}"/></td>
				<td></td>
			</tr>
			
		</table>
	</jsp:body>
</t:layout>