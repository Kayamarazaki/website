<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<t:layout>
	<jsp:body>
		<form name='edit'
		  action="<c:url value='/Account/Change/Password' />" method='POST'>
		  	<div class="form-group">
				<label for="current">Current Password:</label>
				<input type="password" class="form-control" name="current"/>" required>
			</div>
			<div class="form-group">
				<label for="new">New Password:</label>
				<input type="password" class="form-control" name="new"/>" required>
			</div>
			<div class="form-group">
				<label for="repeat">Repeat New Password:</label>
				<input type="password" class="form-control" name="repeat"/>" required>
			</div>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<button type="submit" class="btn btn-default">Save</button>
		</form>
	</jsp:body>
</t:layout>