<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<t:layout>
	<jsp:body>
		<form name='edit'
		  action="<c:url value='/Account/Change/Email' />" method='POST'>
		  	<div class="form-group">
				<label for="email">E-mail Address:</label>
				<input type="text" class="form-control" name="email" value="<c:out value="${current}"/>" required>
			</div>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<button type="submit" class="btn btn-default">Save</button>
		</form>
	</jsp:body>
</t:layout>