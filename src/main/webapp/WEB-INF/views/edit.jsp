<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<t:layout>
	<jsp:body>
		<c:set var="page" value ="/Admin/Edit/"/>
		<form name='edit'
		  action="<c:url value='${page}${name}' />" method='POST'>
		  	<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control" name="name" value="<c:out value="${name}"/>" required>
			</div>
			<div class="form-group">
				<label for="content">Content</label>
				<textarea class="form-control" rows="8" name="content" placeholder="" required><c:out value="${content }"/></textarea>
			</div>
			<input type="hidden" name="originalname" value="<c:out value="${name}"/>"/>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<button type="submit" class="btn btn-default">Save</button>
		</form>
	</jsp:body>
</t:layout>