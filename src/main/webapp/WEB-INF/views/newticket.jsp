<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@page session="true"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:layout>
	<jsp:body>
		<form name='loginForm'
		  action="<c:url value='/Account/Tickets/New' />" method='POST'>
		  	<div class="form-group">
				<label for="subject">Subject</label>
				<input type="text" class="form-control" name="subject" placeholder="Subject" required>
			</div>
			<div class="form-group">
				<label for="message">Message</label>
				<textarea class="form-control" rows="8" name="message" placeholder="Please describe your issue in as much detail as possible" required></textarea>
			</div>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<button type="submit" class="btn btn-default">Submit</button>
		 </form>
	</jsp:body>
</t:layout>