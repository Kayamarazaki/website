<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@page session="true"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:layout>
	<jsp:body>
	<c:if test="${not empty error}">
		<p class="bg-danger">${error}</p>
	</c:if>
	<c:if test="${not empty msg}">
		<p class="bg-info">${msg}</p>
	</c:if>
		<form name='loginForm'
		  action="<c:url value='/login' />" method='POST'>
		  	<div class="form-group">
				<label for="loginEmail">Email address</label>
				<input type="email" class="form-control" name="username" placeholder="Email" required>
			</div>
			<div class="form-group">
				<label for="loginPassword">Password</label>
				<input type="password" class="form-control" name="password" placeholder="Password" required>
			</div>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<button type="submit" class="btn btn-default">Login</button>
		 </form>
	</jsp:body>
</t:layout>