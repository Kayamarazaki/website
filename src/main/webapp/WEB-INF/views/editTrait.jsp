<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<t:layout>
	<jsp:body>
		<c:set var="page" value ="/Admin/Edit/"/>
		<c:set var="slash" value ="/"/>
		<c:set var="q" value="?" />
		<form name='edit' enctype="multipart/form-data"
		  action="<c:url value='${page}${type}${slash}${trait.name}${q}${_csrf.parameterName}=${_csrf.token}' />" method='POST'>
		  	<div class="form-group">
				<label for="newname">Name</label>
				<input type="text" class="form-control" name="newname" value="<c:out value="${trait.name}"/>" required>
			</div>
			<div class="form-group">
				<label for="content">Description</label>
				<textarea class="form-control" rows="3" name="description" placeholder=""><c:out value="${trait.description}"/></textarea>
			</div>
			<div class="form-group">
				<label for="image">Image</label>
				<input type="file" class="form-control" name="file" placeholder="File" accept='image/*'>
			</div>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<button type="submit" class="btn btn-default">Save</button>
		</form>
	</jsp:body>
</t:layout>