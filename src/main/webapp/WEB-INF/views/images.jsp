<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page isELIgnored="false"%>
<%@page session="true"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<t:layout>
	<jsp:body>
	<h1>Images</h1>
	<c:if test="${not empty error}">
		<p class="bg-danger">${error}</p>
	</c:if>
	<c:if test="${not empty info}">
		<p class="bg-info">${info}</p>
	</c:if>
	<table class="table">
		<thead>
			<tr>
				<th>Example</th>
				<th>Path</th>
			</tr>
		</thead>
		<tbody>
			<c:set var="path" value ="/images/"/>
			<c:forEach var="imagename" items="${images}">
				<tr>
					<td>
						
						<a href="<c:url value='${path}${imagename}'/>"><img src="<c:url value='${path}${imagename}'/>" height="50px" width="auto"/></a>
					</td>
					<td>
						http://www.feistyfelinesbreedables.com${path}${imagename}
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="col-md-12">
		<c:set var="upload" value ="/Admin/Images?"/>
		<form enctype="multipart/form-data" name='loginForm'
			  action="<c:url value='${upload}${_csrf.parameterName}=${_csrf.token}' />" method='POST'>
		  	<div class="form-group">
				<label for="file">File</label>
				<input type="file" class="form-control" name="file" placeholder="File" accept='image/*' required>
			</div>
			
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<button type="submit" class="btn btn-default">Upload</button>
		 </form>
	 </div>
	</jsp:body>

</t:layout>